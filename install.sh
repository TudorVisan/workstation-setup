#!/bin/bash
# Usage:
#   bash install.sh [OPTIONS...]
#
# Install and setup the workstation configuration files in this repository to
# your home directory.
#
# Options:
#   -i, --interactive   Prompt before attempting to install each file.
#   -n, --no-clobber    Do not overwrite existing destination files.
#   -f, --force         Always overwrite existing destination files.
#   -x, --uninstall     Uninstall instead of install.
#   -d, --diff          Compare files in setup with installed version.
#   -h, --help          Show this help.

ECHO="echo -ne"
RM="rm"
CP="cp"

SCRIPT_DIR=$(dirname $(readlink -f $0))
SCRIPT_NAME=$(basename $(readlink -f $0))

print_usage()
{
    $ECHO "Usage:\n"
    $ECHO "\tbash $SCRIPT_NAME [OPTIONS...]\n"

    $ECHO "\n"

    $ECHO "Install and setup the workstation configuration files in this"\
        "repository to your home directory.\n"

    $ECHO "\n"

    $ECHO "Options:\n"
    $ECHO "\t-i, --interactive\t- Prompt before attempting to install each"\
        "file\n"
    $ECHO "\t-n, --no-clobber\t- Do not overwrite existing destination files\n"
    $ECHO "\t-f, --force\t\t- Always overwrite existing destination files\n"
    $ECHO "\t-x, --uninstall\t\t- Uninstall instead of install\n"
    $ECHO "\t-d, --diff\t\t- Compare files in setup with installed version\n"
    $ECHO "\t-h, --help\t\t- Show this help\n"
}

print_invalid_option()
{
    local opt=$1

    $ECHO "Invalid option $opt.\n" 1>&2
    $ECHO "Try bash $SCRIPT_NAME -h|--help.\n" 1>&2
}

print_invalid_option_value()
{
    local opt=$1
    local value=$2

    $ECHO "Invalid value \"$value\" for $opt.\n" 1>&2
}

parse_arguments()
{
    while [ $# -gt 0 ]
    do
        local opt=$1
        if [ "${opt:0:1}" == "-" ]
        then
            shift

            case "$opt" in
                -i|--interactive)
                    interactive=true
                    ;;
                -n|--no-clobber)
                    ask_clobber=false
                    clobber=false
                    ;;
                -f|--force)
                    ask_clobber=false
                    clobber=true
                    ;;
                -x|--uninstall)
                    install=false
                    ;;
                -d|--diff)
                    diff=true
                    ;;
                -h|--help)
                    print_usage=true
                    ;;
                *)
                    print_invalid_option "$opt"
                    exit 1
                    ;;
            esac
        else
            args+=("$opt")
            shift
        fi
    done
}

main()
{
    local modules=(\
        "bash"\
        "git"\
        "powerline"\
        "tmux"\
        "vim"\
        )

    local print_usage=false
    local interactive=false
    local ask_clobber=true
    local clobber=false
    local install=true
    local diff=false

    declare -a args
    parse_arguments "$@"

    if $print_usage
    then
        print_usage
        exit 0
    fi

    for module in ${modules[*]}
    do
        local cmd="$SCRIPT_DIR/$module-setup/install.sh"

        if $interactive
        then
            # Ask for interactive session.
            cmd+=" -i"
        fi

        if ! $ask_clobber
        then
            if $clobber
            then
                # Ask for clobbering.
                cmd+=" -f"
            else
                # Ask for no clobbering.
                cmd+=" -n"
            fi
        fi

        if $diff
        then
            # Ask for differences only.
            cmd+=" -d"
        else
            if ! $install
            then
                # Ask to uninstall.
                cmd+=" -x"
            fi
        fi

        $ECHO "Installing $module-setup...\n"
        if ! eval $cmd
        then
            $ECHO "Stopping because of error in module $module-setup.\n" 1>&2
            return 1
        fi
    done
}

main "$@"
