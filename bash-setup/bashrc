############################## Sourced scripts ################################

# Source global bashrc.
if [ -f /etc/bashrc ]
then
    global_bashrc=/etc/bashrc
fi

if [ -n "$global_bashrc" ]
then
    source $global_bashrc
fi

# Source fzf bash keybindings.
if [ -f /usr/share/fzf/shell/key-bindings.bash ]
then
    fzf_key_bindings=/usr/share/fzf/shell/key-bindings.bash
elif [ -f /usr/share/doc/fzf/examples/key-bindings.bash ]
then
    fzf_key_bindings=/usr/share/doc/fzf/examples/key-bindings.bash
elif [ -f $HOME/.fzf.bash ]
then
    fzf_key_bindings=$HOME/.fzf.bash
fi

if [ -n "$fzf_key_bindings" ]
then
    source $fzf_key_bindings
fi

# Source Powerline bindings.
#if [ -f /usr/share/powerline/bash/powerline.sh ]
#then
    #powerline_bindings=/usr/share/powerline/bash/powerline.sh
#elif [ -f /usr/share/powerline/bindings/bash/powerline.sh ]
#then
    #powerline_bindings=/usr/share/powerline/bindings/bash/powerline.sh
#fi

#if [ -n "$powerline_bindings" ]
#then
    #powerline-daemon -q
    #POWERLINE_BASH_CONTINUATION=1
    #POWERLINE_BASH_SELECT=1
    #source $powerline_bindings
#fi

############################## Configuration ##################################

# Append to the history file, don't overwrite it.
shopt -s histappend

# Don't put duplicate lines or lines starting with space in the history.
HISTCONTROL=ignoreboth

############################## Environment variables ##########################

if [ -d $HOME/bin ]
then
    export PATH="$HOME/bin:$PATH"
fi

if [ -d /usr/lib/ccache ]
then
    export PATH="/usr/lib/ccache:$PATH"
fi

if [ -n "$PS1" ]
then
    export PS1="[\u@\h \w]\\$ "
fi

export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8
export LC_ALL=en_US.UTF-8

############################## Aliases ########################################

alias ls="ls --group-directories-first --color=auto"
alias ll="ls -l"

alias grep="grep --color=auto"

# Alias for "less using vim".
if command -v vim &>/dev/null && [ -f $HOME/.vless.vimrc ]
then
    alias vless="env VIMINIT=\"source $HOME/.vless.vimrc\" vim -"
fi
