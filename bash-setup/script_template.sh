#!/usr/bin/env bash
# Usage:
#   bash script_template.sh [OPTIONS...]
#
# Bash script template.
#
# Options:
#   -h, --help          Show this help.

SCRIPT_DIR=$(dirname $(readlink -f $0))
SCRIPT_NAME=$(basename $(readlink -f $0))

print_usage()
{
    printf "Usage:\n"
    printf "\tbash $SCRIPT_NAME [OPTIONS...]\n"

    printf "\n"

    printf "Bash script template.\n"

    printf "\n"

    printf "Options:\n"
    printf "\t-h, --help\t- Show this help\n"
}

# $1 - The invalid option string.
print_invalid_option()
{
    local opt=$1

    printf "Invalid option $opt.\n" 1>&2
    printf "Try bash $SCRIPT_NAME --help.\n" 1>&2
}

# $1 - The option string for the invalid value.
# $2 - The invalid value.
print_invalid_option_value()
{
    local opt=$1
    local value="$2"

    printf "Invalid value \"$value\" for option $opt.\n" 1>&2
    printf "Try bash $SCRIPT_NAME --help.\n" 1>&2
}

# $@ - Arguments.
parse_arguments()
{
    while [ $# -gt 0 ]
    do
        local opt=$1
        if [ "${opt:0:1}" == "-" ]
        then
            shift

            case "$opt" in
                -h|--help)
                    print_usage=true
                    ;;
                *)
                    print_invalid_option "$opt"
                    exit 1
                    ;;
            esac
        else
            args+=("$opt")
            shift
        fi
    done
}

# $1 - Message to be displayed when prompting user for their choice. This is a
# y/N choice.
choice()
{
    local message="$1"

    printf "$message (y/N) "
    read choice garbage

    choice=$(echo $choice | tr "a-z" "A-Z")
    return $([ "$choice" == "Y" ])
}

# $1 - Message to be displayed when prompting user for input. Input is
# considered until a newline ('\n').
input()
{
    local message="$1"

    printf "$message: "
    read input

    printf "$input"
}

main()
{
    local print_usage=false

    declare -a args
    parse_arguments "$@"

    if $print_usage
    then
        print_usage
        exit 0
    fi
}

main "$@"
