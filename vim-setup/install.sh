#!/bin/bash
# Usage:
#   bash install.sh [OPTIONS...]
#
# Install and setup the vim configuration files in this repository to your home
# directory.
#
# Options:
#   -i, --interactive   Prompt before attempting to install each file.
#   -n, --no-clobber    Do not overwrite existing destination files.
#   -f, --force         Always overwrite existing destination files.
#   -x, --uninstall     Uninstall instead of install.
#   -d, --diff          Compare files in setup with installed version.
#   -h, --help          Show this help.

ECHO="echo -ne"
RM="rm"
CP="cp"
CURL="curl -s"
MKDIR="mkdir -p"

if command -v vimdiff &>/dev/null
then
    # Use vimdiff is available.
    DIFF="vimdiff"
else
    # Use diff as a last resort.
    DIFF="diff"
fi

SCRIPT_DIR=$(dirname $(readlink -f $0))
SCRIPT_NAME=$(basename $(readlink -f $0))

print_usage()
{
    $ECHO "Usage:\n"
    $ECHO "\tbash $SCRIPT_NAME [OPTIONS...]\n"

    $ECHO "\n"

    $ECHO "Install and setup the vim configuration files in this repository"\
        "to your home directory.\n"

    $ECHO "\n"

    $ECHO "Options:\n"
    $ECHO "\t-i, --interactive\t- Prompt before attempting to install each"\
        "file\n"
    $ECHO "\t-n, --no-clobber\t- Do not overwrite existing destination files\n"
    $ECHO "\t-f, --force\t\t- Always overwrite existing destination files\n"
    $ECHO "\t-x, --uninstall\t\t- Uninstall instead of install\n"
    $ECHO "\t-d, --diff\t\t- Compare files in setup with installed version\n"
    $ECHO "\t-h, --help\t\t- Show this help\n"
}

print_invalid_option()
{
    local opt=$1

    $ECHO "Invalid option $opt.\n" 1>&2
    $ECHO "Try bash $SCRIPT_NAME -h|--help.\n" 1>&2
}

print_invalid_option_value()
{
    local opt=$1
    local value=$2

    $ECHO "Invalid value \"$value\" for $opt.\n" 1>&2
}

parse_arguments()
{
    while [ $# -gt 0 ]
    do
        local opt=$1
        if [ "${opt:0:1}" == "-" ]
        then
            shift

            case "$opt" in
                -i|--interactive)
                    interactive=true
                    ;;
                -n|--no-clobber)
                    ask_clobber=false
                    clobber=false
                    ;;
                -f|--force)
                    ask_clobber=false
                    clobber=true
                    ;;
                -x|--uninstall)
                    install=false
                    ;;
                -d|--diff)
                    diff=true
                    ;;
                -h|--help)
                    print_usage=true
                    ;;
                *)
                    print_invalid_option "$opt"
                    exit 1
                    ;;
            esac
        else
            args+=("$opt")
            shift
        fi
    done
}

choice()
{
    local message="$1"

    $ECHO "$message (y/N) "
    read choice other

    choice=$(echo $choice | tr "a-z" "A-Z")
    return $([ "$choice" == "Y" ])
}

setup_prerequisites()
{
    local prerequisites="$1"

    for prerequisite in $prerequisites
    do
        case $prerequisite in
            "vim-plug")
                # Install vim-plug.
                $ECHO "Installing vim-plug...\n"
                $MKDIR $HOME/.vim/autoload
                $CURL https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim -o $HOME/.vim/autoload/plug.vim
                ;;
            "undodir")
                # Create directory to store undo files.
                $ECHO "Creating directory to store undo files...\n"
                $MKDIR $HOME/.vim/undo
                ;;
        esac
    done
}

setup_postrequisites()
{
    local postrequisites="$1"

    for postrequisite in $postrequisites
    do
        case $postrequisite in
            "vim-plug")
                # Install plugins using vim-plug.
                $ECHO "Installing plugins...\n"
                env VIMINIT="source $HOME/.$file" vim -c "PlugInstall" -c "quitall" &>/dev/null
                ;;
        esac
    done
}

install()
{
    local interactive=$1
    local ask_clobber=$2
    local clobber=$3

    for i in $(seq 0 $((${#files[*]} - 1)))
    do
        local file=${files[$i]}

        if $interactive
        then
            if ! choice "Install $file?"
            then
                continue
            fi
        fi

        if $ask_clobber
        then
            if [ -e "$HOME/.$file" ] && ! choice "Overwrite "$HOME/.$file"?"
            then
                continue
            fi
        else
            if ! $clobber && [ -e "$HOME/.$file" ]
            then
                continue
            fi
        fi

        local file_prerequisites=${prerequisites[$i]}
        if [ "$file_prerequisites" != "" ]
        then
            # Setup pre-requisites.
            $ECHO "Setting-up pre-requisites for $file...\n"
            setup_prerequisites "$file_prerequisites"
        fi

        $ECHO "Installing $file...\n"
        $CP "$SCRIPT_DIR/$file" "$HOME/.$file"

        local file_postrequisites=${postrequisites[$i]}
        if [ "$file_postrequisites" != "" ]
        then
            # Setup post-requisites.
            $ECHO "Setting-up post-requisites for $file...\n"
            setup_postrequisites "$file_postrequisites"
        fi
    done
}

uninstall()
{
    local interactive=$1

    for i in $(seq 0 $((${#files[*]} - 1)))
    do
        file=${files[$i]}

        if [ -e "$HOME/.$file" ]
        then
            if $interactive && ! choice "Uninstall $HOME/.$file?"
            then
                continue
            fi

            $ECHO "Uninstalling $HOME/.$file\n"
            $RM "$HOME/.$file"
        fi
    done
}

diff()
{
    local interactive=$1

    for i in $(seq 0 $((${#files[*]} - 1)))
    do
        local file=${files[$i]}

        if $interactive
        then
            if ! choice "Diff $file?"
            then
                continue
            fi
        fi

        # Diff the file.
        $ECHO "Diffing $file...\n"
        if ! $DIFF "$SCRIPT_DIR/$file" "$HOME/.$file"
        then
            $ECHO "Stopping because $DIFF encountered an error.\n" 1>&2
            return 1
        fi
    done
}

main()
{
    local files=(\
        "gvimrc"\
        "vimrc"\
        "vless.vimrc"\
        )
    local prerequisites=(\
        ""\
        "vim-plug undodir"\
        "vim-plug"\
        )
    local postrequisites=(\
        ""\
        "vim-plug"\
        "vim-plug"\
        )

    local print_usage=false
    local interactive=false
    local ask_clobber=true
    local clobber=false
    local install=true
    local diff=false

    declare -a args
    parse_arguments "$@"

    if $print_usage
    then
        print_usage
        exit 0
    fi

    if $diff
    then
        diff $interactive
    elif $install
    then
        install $interactive $ask_clobber $clobber
    else
        uninstall $interactive
    fi
}

main "$@"
