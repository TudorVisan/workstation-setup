"""""""""""""""""""""""""""""" Less using vim """""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""" Plugins """"""""""""""""""""""""""""""""""""""""

" Load plugins using vim-plug.
call plug#begin('~/.vim/plugged')

Plug 'tpope/vim-sensible'

Plug 'junegunn/fzf', {'do': {-> fzf#install()}}
Plug 'junegunn/fzf.vim'
nmap <leader>fzf :FZF<CR>

Plug 'scrooloose/nerdtree'

Plug 'henrik/vim-indexed-search'

Plug 'yssl/QFEnter'

Plug 'jremmen/vim-ripgrep'

call plug#end()

"""""""""""""""""""""""""""""" Configuration """"""""""""""""""""""""""""""""""

" Set dark colour scheme.
set background=dark
highlight colorcolumn ctermbg = 8

" This is an optional light colour scheme. Uncomment to use.
"set background=light
"highlight colorcolumn ctermbg = 7


" Tab completion more like the one in bash.
set wildmode=longest,list,full
set wildmenu

" Disable visual wrapping of lines when they exceed the width of the window.
set nowrap

" Enable list mode.
set list

" Show line numbers.
set number

" Highlight all search matches.
set hlsearch

"""""""""""""""""""""""""""""" Auto-commands """"""""""""""""""""""""""""""""""

" Mark buffer as clean.
autocmd StdinReadPost * set nomodified

" Don't show the status line.
autocmd StdinReadPost * set laststatus=0

" Make quickfix window always open at the bottom of the screen.
autocmd FileType qf wincmd J

"""""""""""""""""""""""""""""" Commands """""""""""""""""""""""""""""""""""""""

" Commands to toggle highlighting the cursor line and column.
command CL  setlocal cursorline!
command CC  setlocal cursorcolumn!
command CLC setlocal cursorline! | setlocal cursorcolumn!

"""""""""""""""""""""""""""""" Mappings """""""""""""""""""""""""""""""""""""""

" Map <Ctrl-h>, <Ctrl-j>, <Ctrl-k>, <Ctrl-l> for navigating through windows.
nmap <C-h>  <C-w>h
nmap <C-j>  <C-w>j
nmap <C-k>  <C-w>k
nmap <C-l>  <C-w>l

" Map <Ctrl-n> and <Ctrl-p> for navigating through quickfixes.
nmap <C-n> :cnext<CR>
nmap <C-p> :cprevious<CR>

" Map <leader>o and <leader>O for inserting a new line without entering insert
" mode.
nmap <leader>o o<Esc>
nmap <leader>O O<Esc>

" Pressing q exits.
nmap q :q!<CR>
